import { LitElement, html, css } from "lit";

export class OtherComponent extends LitElement {
  static styles = [
    css`
      :host {
        display: block;
      }
    `,
  ];

  render() {
    return html`<hr />
      <div>
        <h1>Soy el OtherComponent</h1>
      </div>`;
  }
}
