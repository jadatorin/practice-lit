import { LitElement, html, css } from "lit";

export class UserProfile extends LitElement {
  static styles = [
    css`
      :host {
        display: block;
      }
    `,
  ];
  static properties = {
    data: { type: Object },
  };

  constructor() {
    super();
    this.name = "Somebody";
    this.data = {};
  }
  fetchData() {
    return fetch("https://randomuser.me/api")
      .then((res) => res.json())
      .then((data) => {
        const info = data.results[0];

        this.data = {
          name: info.name.title + " " + info.name.first,
          surname: info.name.last,
          email: info.email,
          login: info.login.username,
          location: info.location.country + " " + info.location.city,
          password: info.login.password,
          gender: info.gender,
          age: info.dob.age,
          picture: info.picture.large,
        };
        console.log(this.data);
      });
  }

  firstUpdated() {
    this.fetchData();
    this.saludar();
    console.log("firstUpdated!!");
  }
  saludar() {
    const hola = "hello";
    return hola;
  }
  renderLoading() {
    return html`<img src="spinner.gif" alt="loading" />`;
  }

  render() {
    const user = this.data;
    return !user.name
      ? this.renderLoading()
      : html`
          <h1>${this.saludar()}</h1>
          <div class="user">
            <img class="picture" src="${user.picture}" alt="${user.name}" />
            <div class="info">
              <h1>${user.name} ${user.surname}</h1>
              <div><strong>Email</strong>: <span>${user.email}</span></div>
              <div>
                <strong>Location</strong>: <span>${user.location}</span>
              </div>
              <div>
                <strong>Gender</strong>: ${user.gender} -
                <strong>Age:</strong> ${user.age}
              </div>
            </div>
          </div>
        `;
  }
}
