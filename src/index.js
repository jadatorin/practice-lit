import { UserProfile } from "./UserProfile.js";
import { OtherComponent } from "./otherComponent.js";

customElements.define("user-profile", UserProfile);
customElements.define("other-component", OtherComponent);
